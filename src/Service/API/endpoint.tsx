// export const BASE_URL = process.env.REACT_APP_GITHUB_BASE_URL;
//  tại sao phải xài process.env.REACT_APP_GITHUB_BASE_URL ( tối ưu )79

const BASE_URL = process.env.REACT_APP_GITHUB_BASE_URL;

const ENDPOINTS = {
  SEARCH_USERS: '/search/users',
};

const CONFIGS = {
  BASE_URL,
  ENDPOINTS,
};

export default CONFIGS;
