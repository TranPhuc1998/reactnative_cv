import { LinearGradient as Gradient } from 'expo-linear-gradient';
import LottieView from 'lottie-react-native';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, Image, Animated, TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Svg, { Defs, LinearGradient, Stop, Path } from 'react-native-svg';
import { Transition } from 'react-navigation-fluid-transitions';
import { Ionicons } from '@expo/vector-icons';
import TypingText from 'react-native-typical';

interface User {
  props: any;
  doAnimation: number;
  setDoAnimation: number;
  password: string;
  email: string;
}
const { height, width } = Dimensions.get('window');

const App: React.FC = (props) => {
  const [doAnimation, setDoAnimation] = useState<User>();
  const [email, setEmail] = useState<User>('');
  const [password, setPassword] = useState<User>('');
  const [animation, isEditing] = useState(new Animated.Value(0));

  const AnimatedImage = (doAnimation) => {
    return (
      <Animated.Image
        style={[
          styles.doctorImage,
          {
            transform: [
              /*{
                translateX: animation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 20]
                }),
              },
              {
                translateY: animation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, -20],
                }),
              },
              {
                scale: animation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 0.5],
                }),
              },*/
            ],
          },
        ]}
        source={require('../../assets/image/doctor-female-plant.png')}
      />
    );
  };

  /*useEffect(() => {
    Animated.timing(animation, {
      //toValue: 1,
      toValue: isEditing ? 1 : 0,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }, [doAnimation]);*/

  return (
    <View style={styles.container}>
      <Transition shared="topImage">
        <Image
          style={styles.doctorImage}
          source={require('../../assets/image/doctor-female-plant.png')}
        />
      </Transition>
      <Transition shared="topSvg">
        <View style={styles.svgBackground}>
          <Svg width={width} height={width} viewBox="0 0 375 340">
            <Defs>
              <LinearGradient x1="90.743%" y1="87.641%" x2="10.14%" y2="3.465%" id="prefix__a">
                <Stop stopColor="#5851DB" offset="0%" />
                <Stop stopColor="#C13584" offset="100%" />
              </LinearGradient>
            </Defs>
            <Path
              d="M.11-2H376c-.005 204.081-.005 306.134 0 306.158-95.114 82-135.593-8.28-188-16.789C98.06 266.778 51.482 346.402.11 262.41-.037 251.212-.037 163.075.11-2z"
              fill="url(#prefix__a)"
              fillRule="evenodd"
            />
          </Svg>
        </View>
      </Transition>
      <Transition shared="topImage">
        <Image style={styles.doctorImage} source={require('../../assets/image/2.png')} />
      </Transition>
      <Transition appear="scale">
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          style={styles.closeButton}>
          <Ionicons name="ios-arrow-back" size={40} color="#FFFFFF" />
        </TouchableOpacity>
      </Transition>
      <Transition appear="horizontal">
        <View style={styles.logo}>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
            {/* <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            colors={['#5851DB', '#C13584', '#E1306C', '#FD1D1D', '#F77737']}
            style={styles.instagramButton}>
            <Text style={{color: 'white'}}>Sign In With Instagram</Text>
          </LinearGradient> */}
            <Text style={styles.logoTextBig}>Call Me Chảy</Text>
          </View>

          <Text style={styles.logoSlogan}>This is my profile</Text>
        </View>
      </Transition>

      {/* Body */}
      <View style={styles.ctaWrapper}>
        <View style={[styles.textInputWrapper, { borderWidth: 0 }]}>
          {/* <TouchableOpacity activeOpacity={0.7} style={[styles.ctaButtonWrapper, {}]}>
            <Gradient
              colors={['#3186FF', '#53B4FF']}
              start={[0, 1]}
              end={[1, 0]}
              style={styles.ctaButton}>
              <Text style={styles.ctaButtonText}>Login</Text>
            </Gradient>
          </TouchableOpacity> */}
          {/* <Text style={{ fontWeight: '900', fontSize: 30 }}></Text> */}
          <TypingText
            steps={[' ', 0, 'You can call me Chảy', 100]}
            loop={Infinity}
            editDelay={80}
            deleteDelay={10}
            style={{ fontWeight: '700', fontSize: 30, color: '#5851DB' }}
          />
          <TypingText
            steps={['I am developer 💻', 50, 'But huge fan animation of React Native 🚀', 50]}
            loop={Infinity}
            blinkCursor={true}
            editDelay={80}
            deleteDelay={10}
            style={{ fontSize: 15, fontWeight: '400' }}
          />
          <TypingText
            steps={['Love Soccer⚽️', 50, 'My website team Vlinteamsoccer.firebaseapp.com 👈🏻', 50]}
            loop={Infinity}
            blinkCursor={true}
            editDelay={80}
            deleteDelay={10}
            style={{ fontSize: 15, fontWeight: '400' }}
          />
        </View>
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
  },
  doctorImage: {
    borderRadius: 10,
    position: 'absolute',
    left: -30,
    top: 30,
    width: (width / 100) * 45,
    height: (width / 100) * 65,
    transform: [
      {
        translateX: 20,
      },
      {
        translateY: -20,
      },
      {
        scale: 0.5,
      },
    ],
  },
  closeButton: {
    position: 'absolute',
    top: 40,
    left: 15,
    width: 40,
    height: 40,
    //backgroundColor: '#FF00FF20',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  svgBackground: {
    position: 'absolute',
    top: -150,
  },
  logo: {
    position: 'absolute',
    top: 60,
    right: 0,
    width: (width / 100) * 65,
    height: (width / 100) * 30,
    //backgroundColor: '#FF00FF20',
  },
  logoTextBig: {
    fontSize: 40,
    fontWeight: '600',
    color: '#FFFFFF',
    letterSpacing: -4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoTextThin: {
    fontSize: 33,
    fontWeight: '300',
    color: '#FFFFFF',
    letterSpacing: -2,
    paddingTop: 20,
  },
  logoSlogan: {
    fontSize: 20,
    fontWeight: '200',
    color: '#FFFFFF',
    letterSpacing: -0.3,
    paddingTop: 0,
    paddingLeft: 10,
  },
  ctaWrapper: {
    flex: 1,
    width: '100%',
    margin: 20,
    marginTop: (height / 100) * 28,
  },
  ctaMessage: {
    fontSize: 30,
    fontWeight: '600',
    color: '#21232A',
    letterSpacing: -1,
    lineHeight: 35,
  },
  ctaMessageAction: {
    fontSize: 24,
    fontWeight: '500',
    color: '#B1B9C0',
    letterSpacing: -1,
    lineHeight: 30,
  },
  textInputWrapper: {
    width: '100%',
    height: 68,
    marginTop: 20,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#C2C2C2',
  },
  textInput: {
    fontSize: 20,
    fontWeight: '600',
    color: '#21232A',
    width: '100%',
    height: 68,
    paddingLeft: 20,
    paddingRight: 20,
  },
  ctaButtonWrapper: {
    shadowColor: '#B1B9C0',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,

    elevation: 16,
  },
  ctaButton: {
    width: '100%',
    height: 68,
    marginTop: 20,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ctaButtonText: {
    fontSize: 22,
    color: '#fff',
  },
  ctaDisclaimer: {
    fontSize: 14,
    fontWeight: '300',
    color: '#3186FF',
    letterSpacing: 0,
    textAlign: 'center',
    lineHeight: 18,
  },
  divisor: {
    width: '100%',
    height: 1,
    backgroundColor: '#F1EEEE',
    marginTop: 30,
    marginBottom: 30,
  },
  doctorButton: {
    width: (width / 100) * 90,
    height: 56,
    margin: 20,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#3186FF',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#7E88C4',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,
  },
  doctorButtonText: {
    fontSize: 22,
    color: '#3186FF',
  },
});

// import React, { useState } from 'react';
// import { SafeAreaView, Text, View, StyleSheet, Dimensions } from 'react-native';

// import {
//   CodeField,
//   Cursor,
//   useBlurOnFulfill,
//   useClearByFocusCell,
// } from 'react-native-confirmation-code-field';

// const CELL_COUNT = 6;
// interface UnderlineExample {}
// const UnderlineExample: React.FC<UnderlineExample> = () => {
//   const [value, setValue] = useState('');
//   const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
//   const [props, getCellOnLayoutHandler] = useClearByFocusCell({
//     value,
//     setValue,
//   });

//   return (
//     <SafeAreaView style={styles.root}>
//       <Text style={styles.title}>Underline example</Text>
//       <View style={{ paddingHorizontal: 30 }}>
//         <CodeField
//           ref={ref}
//           {...props}
//           value={value}
//           onChangeText={setValue}
//           cellCount={CELL_COUNT}
//           rootStyle={styles.codeFieldRoot}
//           keyboardType="number-pad"
//           textContentType="oneTimeCode"
//           renderCell={({ index, symbol, isFocused }) => (
//             <View
//               onLayout={getCellOnLayoutHandler(index)}
//               key={index}
//               style={[styles.cellRoot, isFocused && styles.focusCell]}>
//               <Text style={styles.cellText}>{symbol || (isFocused ? <Cursor /> : null)}</Text>
//             </View>
//           )}
//         />
//       </View>
//     </SafeAreaView>
//   );
// };

// export default UnderlineExample;
// const { height, width } = Dimensions.get('screen');
// const styles = StyleSheet.create({
//   root: { padding: 20, minHeight: 300 },
//   title: { textAlign: 'center', fontSize: 30 },
//   codeFieldRoot: {
//     marginTop: 20,
//     width: width * 0.7,
//     marginLeft: 'auto',
//     marginRight: 'auto',
//     //paddingRight: 2,
//     // backgroundColor: '#FF00FF20',

//     alignSelf: 'center',
//     alignItems: 'center',
//   },
//   cellRoot: {
//     width: 30,
//     height: 60,
//     justifyContent: 'center',
//     alignItems: 'center',
//     borderBottomColor: '#ccc',
//     borderBottomWidth: 1,
//     paddingHorizontal: 5,
//   },
//   cellText: {
//     color: '#000',
//     fontSize: 36,
//     textAlign: 'center',
//   },
//   focusCell: {
//     borderBottomColor: '#FF00FF20',
//     borderBottomWidth: 1,
//   },
// });
